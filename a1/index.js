function addNumbers (num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of 5 and 15:")
	console.log(sum);
}

addNumbers(5, 15);

function subtractNumbers (num1, num2){
	let difference = num1 - num2;
	console.log("Displayed difference of 20 and 5:")
	console.log(difference);
}

subtractNumbers(20, 5);

function multiplyNumbers (num1, num2){
	return num1 * num2;
	// console.log("The product of 50 and 10:")
	// console.log(product);
}


function divideNumbers (num1, num2){
	return num1 / num2;
	// console.log("The product of 50 and 10:")
	// console.log(product);
}

let product = multiplyNumbers(50, 10);
let quotient = divideNumbers(50, 10);

console.log("The product of 50 and 10:");
console.log(product);

console.log("The quotient of 50 and 10:");
console.log(quotient);

// let rad = prompt("Input Radius");
// let pi = 3.14;
// function circleArea1(rad){
// 	return pi * rad * rad;
// }

// console.log("The result of getting the area of a circle with " + rad + " radius:");
// let circleArea = circleArea1(rad);
// console.log(circleArea);


let pi = 3.14;
function circleArea1(rad){
	return pi * rad * rad;
}

console.log("The result of getting the area of a circle with 5 radius:");
let circleArea = circleArea1(5);
console.log(circleArea);

// let score1 = prompt("Input score1:");
// let score2 = prompt("Input score2:");
// let score3 = prompt("Input score3:");
// let score4 = prompt("Input score4:");

// function averageScore(score1, score2, score3, score4){
// 	return score1 + score2 + score3 + score4 / 4;
// }

// let averageVar = averageScore(score1, score2, score3, score4);
// console.log("The average score of " + score1 + ", " + score2 + ", " + score3 + ", and " +  score4 + ":");
// console.log(averageVar);


function averageScore(score1, score2, score3, score4){
	return (score1 + score2 + score3 + score4) / 4;
}

let averageVar = averageScore(20, 40, 60, 80);
console.log("The average score of 20, 40, 60, and 80:" );
console.log(averageVar);

function percentage(myScore, passingPercentage){
	let myScorePercentage = (myScore/passingPercentage)*100;
	let isPassed = myScorePercentage > 75;
	return isPassed;
	
}

let isPassingScore = percentage(38, 50);
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);